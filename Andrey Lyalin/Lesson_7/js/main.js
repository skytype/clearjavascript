/*
var item = {
	url: "http://desktopwallpapers.org.ua/mini/201507/40069.jpg",
	name: "CHEVROLET",
	params : "true=>80",
	description : "be conveyed to ...",
	date : "2015/01/25 14:15"
};
*/

function transformArray(Arr) {
	return Arr.map(function (item) {
		return {
			name: chaneCase(item.name),
			url: "http://" + item.url,
			params: gluingObject(item.params),
			description: lineCutting(item.description),
			date: formatDate(item.date)
		};
	});
};

function chaneCase(str) {
	var firstChar = str.charAt(0).toUpperCase();
	var remain = str.substring(1).toLowerCase();
	return firstChar + remain;
};

function lineCutting(str) {
	return str.substr(0, 15) + "...";
};

function gluingObject(object) {
	return object.status + " => " + object.progress;
};

function formatDate(date) {
	var tmpDate = new Date(date);
	function getCorrectDate(val) {
		if(val <10){
			return "0" + val;
		} else {return val;}
	}
	return tmpDate.getFullYear() + "/" +
		getCorrectDate(tmpDate.getMonth() + 1) + "/" +
		getCorrectDate(tmpDate.getDate()) + " " +
		getCorrectDate(tmpDate.getHours()) + ":" +
		getCorrectDate(tmpDate.getMinutes());
};

function firstRow(item, elemRow) {
	var resultHTML = "";

	var itemTemplate = '<div class="col-sm-3 col-xs-6">\
				<img src="$url" alt="$name" class="img-thumbnail">\
				<div class="info-wrapper">\
					<div class="text-muted">$name</div>\
					<div class="text-muted">$description</div>\
                    <div class="text-muted">$params</div>\
					<div class="text-muted">$date</div>\
				</div>\
			</div>';

	resultHTML = itemTemplate
		.replace(/\$name/gi, item.name)
		.replace("$url", item.url)
		.replace("$params", item.params)
		.replace("$description", item.description)
		.replace("$date", item.date);

	elemRow.innerHTML += resultHTML;
};

function secondRow(item, elemRow) {
	var itemTemplate = `<div class="col-sm-3 col-xs-6">\
				<img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
				<div class="info-wrapper">\
					<div class="text-muted">${item.name}</div>\
					<div class="text-muted">${item.description}</div>\
                    <div class="text-muted">${item.params}</div>\
					<div class="text-muted">${item.date}</div>\
				</div>\
			</div>`;

	elemRow.innerHTML += itemTemplate;
};

function lineCreateElement(item, elemRow) {
	var elemDiv = document.createElement('div');
	elemDiv.className = "col-sm-3 col-xs-6";
	elemRow.appendChild(elemDiv);

	var elemImg = document.createElement('img');
	elemImg.setAttribute("src", item.url);
	elemImg.setAttribute("alt", item.name);
	elemImg.className = "img-thumbnail";
	elemDiv.appendChild(elemImg);

	var elemDivWrapper = document.createElement('div');
	elemDivWrapper.className = "info-wrapper";
	elemDiv.appendChild(elemDivWrapper);

	var elemDivTextName = document.createElement('div');
	elemDivTextName.innerHTML = item.name;
	elemDivWrapper.appendChild(elemDivTextName);

	var elemDivTextDescription = document.createElement('div');
	elemDivTextDescription.innerHTML = item.description;
	elemDivWrapper.appendChild(elemDivTextDescription);

	var elemDivTextParams = document.createElement('div');
	elemDivTextParams.innerHTML = item.params;
	elemDivWrapper.appendChild(elemDivTextParams);

	var elemDivTextDate = document.createElement('div');
	elemDivTextDate.innerHTML = item.date;
	elemDivWrapper.appendChild(elemDivTextDate);
};


var objectTransferred = transformArray(data);
function runGallery() {
	var first = document.querySelector('#first-line');
	var second = document.querySelector('#second-line');
	var third = document.querySelector('#third-line');

	objectTransferred.forEach(function (item, index) {
		if (index < 3) {
			var firstLineGallery = firstRow(item, first);
		}
		else if(index < 6){
			var secondLineGallery = secondRow(item, second);
		}
		else if(index < 9){
			var thirdLineGallery = lineCreateElement(item, third);
		}
	});
};
runGallery(objectTransferred);